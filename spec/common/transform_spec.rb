require 'spec_helper'

module Common

  describe Transform2D do

    let!(:transform) { Common::Transform2D.new }
    context "#initialize" do
      it "should be a Common module" do
        expect(Transform2D).to be Common::Transform2D
      end

      it "should have a position of 0,0 and bearing of 0 by default" do
        transform = Common::Transform2D.new
        expect(transform.position.x.to_i).to be 0
        expect(transform.position.y.to_i).to be 0
        expect(transform.bearing.to_i).to be 0
      end
    end

    context "#propagate" do
      it "predicts the new position after step of given magnitude" do
        pos = Common::Vector2D.new(1,1)
        transform = Common::Transform2D.new(:position => pos)
        predicted = transform.propagate(1)
        expect(predicted.x.to_i).to be 2
        expect(predicted.y.to_i).to be 1
      end
    end

    context "#rotate" do
      it "rotates the bearing by given angle" do
        pos = Common::Vector2D.new(1,1)
        transform = Common::Transform2D.new(:bearing => Math::PI/2)
        transform.rotate(-Math::PI)
        expected = 3*Math::PI/2
        expect(Common.float_compare(transform.bearing, expected,0.001))
      end
    end

  end
end
