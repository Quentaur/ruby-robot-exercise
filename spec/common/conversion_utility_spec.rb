require 'spec_helper'

module Common

  context "#deg_to_cardinal_direction" do
    it "should convert deg to cardinal direction" do
      expect(Common.deg_to_cardinal_direction(360)).to eq :east
      expect(Common.deg_to_cardinal_direction(22)).to eq :east
      expect(Common.deg_to_cardinal_direction(67)).to eq :northeast
      expect(Common.deg_to_cardinal_direction(112)).to eq :north
      expect(Common.deg_to_cardinal_direction(157)).to eq :northwest
      expect(Common.deg_to_cardinal_direction(202)).to eq :west
      expect(Common.deg_to_cardinal_direction(247)).to eq :southwest
      expect(Common.deg_to_cardinal_direction(292)).to eq :south
      expect(Common.deg_to_cardinal_direction(337)).to eq :southeast
    end
  end

  context "#cardinal_direction_to_deg" do
    it "should convert cardinal direction to deg" do
      expect(Common.cardinal_direction_to_deg(:east)).to eq 0
      expect(Common.cardinal_direction_to_deg(:northeast)).to eq 45
      expect(Common.cardinal_direction_to_deg(:north)).to eq 90
      expect(Common.cardinal_direction_to_deg(:northwest)).to eq 135
      expect(Common.cardinal_direction_to_deg(:west)).to eq 180
      expect(Common.cardinal_direction_to_deg(:southwest)).to eq 225
      expect(Common.cardinal_direction_to_deg(:south)).to eq 270
      expect(Common.cardinal_direction_to_deg(:southeast)).to eq 315
    end
  end

  context "#rad_to_deg" do
    it "should convert rad to deg" do
      expect(Common.float_compare(Common.to_rad(180), Math::PI, 0.001))
    end
  end

  context "#deg_to_rad" do
    it "should convert deg to rad" do
      expect(Common.float_compare(Common.to_deg(Math::PI), 180.00, 0.001))
    end
  end

end
