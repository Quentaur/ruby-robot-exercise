require 'spec_helper'

module RobotSim
  describe Controller do

    let!(:robot) { RobotSim::Robot.new }
    let!(:control) { RobotSim::Controller.new({robot: robot}) }
    let!(:table) { Environment::Table.new  }

    it "should be a RobotSim module" do
      expect(Controller).to be RobotSim::Controller
    end

    it "should start the robot_sim and initiate the table" do
      expect(control.robot).to be robot
    end

    context "#initialize" do
      it "should initialize the robot_sim with a custom table" do
        robot = RobotSim::Robot.new({table: table})
        controls = RobotSim::Controller.new({robot: robot})
        expect(controls.robot.table.object_id).to be table.object_id
      end
    end

    context "#process_command" do

      it "command 'RIGHT' instructs robot to turn right" do
        control.process_command('PLACE 0,0,NORTH')
        control.process_command('RIGHT')
        expect(control.process_command('REPORT')).to eq [0, 0, "EAST"]
      end

      it "command 'LEFT' instructs robot to turn left" do
        control.process_command('PLACE 0,0,NORTH')
        control.process_command('LEFT')
        expect(control.process_command('REPORT')).to eq [0, 0, "WEST"]
      end

      it "command 'PLACE X,Y,DIRECTION' instructs robot to reposition and reorient" do
        control.process_command('PLACE 1,4,SOUTH')
        expect(control.process_command('REPORT')).to eq [1, 4, "SOUTH"]
      end

      it "command 'MOVE' instructs robot to move forward" do
        control.process_command('PLACE 0,0,NORTH')
        control.process_command('MOVE')
        expect(control.process_command('REPORT')).to eq [0, 1, "NORTH"]
      end
    end

    context "#process_command with invalid commands" do
      it "should return nil" do
          expect(control.process_command("unknown")).to be nil
      end

      it "should return nil" do
        expect(control.process_command("")).to be nil
      end
    end

    context "Replicate required behaviour based on examples provided" do
      it "example a)" do
        control.process_command('PLACE 0,0,NORTH')
        control.process_command('MOVE')
        expect(control.process_command('REPORT')).to eq [0, 1, "NORTH"]
      end
      it "example b)" do
        control.process_command('PLACE 0,0,NORTH')
        control.process_command('LEFT')
        expect(control.process_command('REPORT')).to eq [0, 0, "WEST"]
      end
      it "example c)" do
        control.process_command('PLACE 1,2,EAST')
        control.process_command('MOVE')
        control.process_command('MOVE')
        control.process_command('LEFT')
        control.process_command('MOVE')
        expect(control.process_command('REPORT')).to eq [3, 3, "NORTH"]
      end
    end
  end
end
