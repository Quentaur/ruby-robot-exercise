require 'spec_helper'

module RobotSim

  describe Parser do
    def compare_instruction(instruct_a, instruct_b)
      expect(instruct_a.rotate).to eq instruct_b.rotate
      expect(instruct_a.step).to eq instruct_b.step
      expect(instruct_a.reposition).to eq instruct_b.reposition
      expect(instruct_a.reorient).to eq instruct_b.reorient
      expect(instruct_a.report).to eq instruct_b.report
    end

    let!(:test_parser) { RobotSim::Parser.new }
    context "#initialize" do
      it "should be a RobotSim module" do
        expect(Parser).to be RobotSim::Parser
      end
    end

    context "#parse" do
      context "returns the correct instruction for the following commands:" do
        it "place" do
          pos = Common::Vector2D.new(1,2)
          expected = Instruction.new(:reposition => pos, reorient: Math::PI)
          instruct = test_parser.parse("PLACE 1,2,WEST")
          compare_instruction(instruct, expected)
        end

        it "move" do
          expected = Instruction.new(:step => 1)
          instruct = test_parser.parse("MOVE")
          compare_instruction(instruct, expected)
        end

        it "left" do
          expected = Instruction.new(:rotate => Math::PI/2)
          instruct = test_parser.parse("LEFT")
          compare_instruction(instruct, expected)
        end

        it "right" do
          expected = Instruction.new(:rotate => -Math::PI/2)
          instruct = test_parser.parse("RIGHT")
          compare_instruction(instruct, expected)
        end

        it "report" do
          expected = Instruction.new(:report => true)
          instruct = test_parser.parse("REPORT")
          compare_instruction(instruct, expected)
        end

      end
    end

  end
end
