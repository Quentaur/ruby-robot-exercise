require_relative "../spec_helper.rb"

module RobotSim
  describe Robot do

    let!(:robot) { RobotSim::Robot.new }
    
    it "should be a RobotSim module" do
      expect(Robot).to be RobotSim::Robot
    end

    context "#initialize" do

      it "is expected to have a table by default" do
        expect(robot.table).to_not be nil
      end

      it "is expected to have a customised table" do
        surface = Environment::Table.new({width: 11, length: 7, origin: Common::Vector2D.new(0,0)})
        robot = RobotSim::Robot.new({table: surface})
        expect(robot.table.width).to be 11
        expect(robot.table.length).to be 7
      end
    end

    context "#reposition" do
      it "Ignores move commands prior to successful placement on the table" do
        instruct = RobotSim::Instruction.new({:step => 1})
        expect(robot.handle_instruction(instruct)).to eq("\nIgnoring commands until successfully placed on table.\n")
      end

      it "Ignores rotation commands prior to successful placement on the table" do
        instruct = RobotSim::Instruction.new({:rotate => 1})
        expect(robot.handle_instruction(instruct)).to eq("\nIgnoring commands until successfully placed on table.\n")
      end

      it "Ignores reorientation commands prior to successful placement on the table" do
        instruct = RobotSim::Instruction.new({:reorient => 1})
        expect(robot.handle_instruction(instruct)).to eq("\nIgnoring commands until successfully placed on table.\n")
      end

      it "Ignores report commands prior to successful placement on the table" do
        instruct = RobotSim::Instruction.new({:report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nIgnoring commands until successfully placed on table.\n")
      end

      it "repositions robot to (x, y) coordinates" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(1,1),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([1, 1, "NORTH"])
      end

      it "attempting to place robot over table width is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(20,1),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end

      it "attempting to place robot under table width is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(-20,1),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end

      it "attempting to place robot over table length is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(1,20),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end

      it "attempting to place robot under table length is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(1,-20),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end

      it "attempting to place robot over table length and width is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(20, 20),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end

      it "attempting to place robot under table length and width is ignored" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(-20, -20),
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq("\nPLACE command ignored, robot cannot be placed off the table.\n")
      end
    end

    context "#turning" do
      it "should rotate the robot 90 deg clockwise without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :rotate => -Math::PI/2,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "EAST"])
      end

      it "should simplify to rotate the robot 90 deg clockwise without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :rotate => -5 * Math::PI/2,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "EAST"])
      end

      it "should rotate the robot 90 deg counterclockwise without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :rotate => Math::PI/2,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "WEST"])
      end

      it "should simplify to rotate the robot 90 deg counterclockwise without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :rotate => 5 * Math::PI/2,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "WEST"])
      end
    end

    context "#reorient" do
      it "should rotate the robot to the specified bearing without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :reorient => 3*Math::PI/2,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "SOUTH"])
      end

      it "should simplify to rotate the robot to the specified bearing without changing the position" do
        instruct = RobotSim::Instruction.new({:reposition => Common::Vector2D.new(0, 0),
                                              :reorient => 3*Math::PI/2 + 2*Math::PI,
                                              :report => true})
        expect(robot.handle_instruction(instruct)).to eq([0, 0, "SOUTH"])
      end
    end

    context "#movement" do

      it "should move the robot forward by the step size" do
        start = Common::Vector2D.new(1,1)
        instruct = RobotSim::Instruction.new({:reposition => start,
                                              :step => 1,
                                              :report => true})

        expect(robot.handle_instruction(instruct)).to eq([1, 2, "NORTH"])
      end

      it "should ignore move commands that would lead it off the table" do
        start = Common::Vector2D.new(1,1)
        instruct = RobotSim::Instruction.new({:reposition => start,
                                              :step => -2,
                                              :report => true})

        expect(robot.handle_instruction(instruct)).to eq("\nMOVE command ignored, robot cannot move off the table.\n")
      end

      it "should ignore move commands that would lead it off the table" do
        start = Common::Vector2D.new(1,1)
        instruct = RobotSim::Instruction.new({:reposition => start,
                                              :rotate => Math::PI,
                                              :step => 2,
                                              :report => true})

        expect(robot.handle_instruction(instruct)).to eq("\nMOVE command ignored, robot cannot move off the table.\n")
      end
    end

  end
end
