require 'spec_helper'

module Environment
  describe Table do
    let!(:table) { Environment::Table.new }
    context "#initialize" do
      it "should be an Environment module" do
        expect(Table).to be Environment::Table
      end

      it "have width and length of 5 by default" do
        expect(table.width).to be 5
        expect(table.length).to be 5
      end
    end

    context "#initialize with options" do
      it "should initialize the options" do
        width = 10
        length = 20
        origin = Common::Vector2D.new(-20,-20)

        custom_table = Environment::Table.new({width: width, length: length, origin: origin })
        expect(custom_table.width).to be width
        expect(custom_table.length).to be length
        expect(custom_table.origin.x).to be origin.x
        expect(custom_table.origin.y).to be origin.y
      end
    end

  end
end
