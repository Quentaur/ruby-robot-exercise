require 'spec_helper'

describe Common do
  it 'has a version number' do
    expect(Common::VERSION).not_to be nil
  end
end

describe Environment do
  it 'has a version number' do
    expect(Environment::VERSION).not_to be nil
  end
end

describe RobotSim do
  it 'has a version number' do
    expect(RobotSim::VERSION).not_to be nil
  end
end