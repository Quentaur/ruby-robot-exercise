$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)

Dir[File.dirname(__FILE__) + '/../lib/common/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/../lib/environment/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/../lib/robot_sim/*.rb'].each {|file| require file }
