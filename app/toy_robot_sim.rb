
require_relative "../lib/robot_sim/robot_sim.rb"
require_relative "../lib/robot_sim/controller.rb"

puts "\n******************************************************************"
puts "*                                                                *"
puts "*                                         |___|                  *"
puts "*                                        [ === ]                 *"
puts "*           TOY ROBOT SIMULATOR            |_|                   *"
puts "*                v 0.1.0              ____|   |____              *"
puts "*                                     || | o=o | ||              *"
puts "*               by Q Cooke           <[] | o=o | []>             *"
puts "*                                        |=====|                 *"
puts "*                                       | | | | |                *"
puts "*                                      <_________>               *"
puts "******************************************************************"

control = RobotSim::Controller.new

puts "\nSupported commands are as follows: \n"
puts "  ==> RIGHT || LEFT || MOVE || REPORT || PLACE X,Y,DIRECTION \n"
puts "\nPLACE robot on table to start eg. PLACE 0,0,NORTH \n"

loop do
  puts "\n => Enter command:"
  command = gets.chomp
  puts control.process_command(command)
end