require_relative "../common/conversion_utility.rb"
require_relative "../environment/table.rb"

module RobotSim
  class Instruction

    attr_reader :rotate, :step, :reposition, :reorient, :report

    def initialize(options={})
      @rotate = options[:rotate] || 0
      @step = options[:step] || 0
      @reposition = options[:reposition] || nil #Common::Vector2D
      @reorient = options[:reorient] || nil
      @report = options[:report] || nil
    end

  end

  class Robot

    attr_reader :table, :state, :is_placed

    def initialize(options={})
      @table = options[:table] || Environment::Table.new
      @state = Common::Transform2D.new(:bearing => Math::PI/2)
      @is_placed = false
    end

    def handle_instruction(instruct)
      if(instruct.reposition)
        if !place(instruct.reposition)
          return "\nPLACE command ignored, robot cannot be placed off the table.\n"
        else
          @is_placed = true
        end
      end

      if(!@is_placed)
        return "\nIgnoring commands until successfully placed on table.\n"
      else
        handle_secondary_instructions(instruct)
      end
    end


    private

    def handle_secondary_instructions(instruct)
      if(instruct.reorient)
        @state.bearing = instruct.reorient
      end

      @state.rotate(instruct.rotate)

      if(instruct.step != 0)
        projection = state.propagate(instruct.step)
        if !place(projection)
          return "\nMOVE command ignored, robot cannot move off the table.\n"
        end
      end

      if(instruct.report)
        report_state
      end
    end

    def place(point)
      if(@table.is_point_on_table(point))
        @state.position = point
        return true
      else
        return false
      end
    end

    def report_state(*_)
      [@state.position.x.to_i, @state.position.y.to_i, Common.rad_to_cardinal_direction(@state.bearing)]
    end

  end
end
