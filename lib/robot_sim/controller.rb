require_relative "../environment/table.rb"
require_relative "parser.rb"

module RobotSim
  class Controller

    attr_reader :robot, :name, :parser

    def initialize(options={})
      @table  =  options[:board] || Environment::Table.new
      @robot  =  options[:robot] || Robot.new({table: @table})
      @parser =  Parser.new
    end

    def process_command(command)
        instruct = parser.parse(command)
        if(instruct)
          robot.handle_instruction(instruct)
        end
    end

  end
end
