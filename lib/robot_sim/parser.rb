require_relative "../common/conversion_utility.rb"

module RobotSim
  class Parser
    attr_reader :commands

    def initialize(options={})
      @delimiter = options[:delim] || ","
      @instruction_database = options[:commands] || {
        "move" => Instruction.new(:step => 1),
        "left" => Instruction.new(:rotate => +Math::PI/2),
        "right" => Instruction.new(:rotate => -Math::PI/2),
        "report" => Instruction.new(:report => true)
      }
    end

    def sanitise_input(input)
      input.to_s.downcase
    end

    def commands_reminder
      puts "\n  Supported commands are as follows: \n"
      puts "==> RIGHT || LEFT || MOVE || REPORT || PLACE X,Y,DIRECTION \n"
    end

    def parse(message)

      instruct = nil

      if message.to_s == ""
        puts "No command given. Please try again."
        commands_reminder
      else
        input = message.split
        command = sanitise_input(input[0])
        instruct = @instruction_database[command]

        if(!instruct)
          if(command == "place")

            @arguments = []
            extract_arguments(input[1], @delimiter)
            instruct = Instruction.new(:reposition => Common::Vector2D.new(@arguments[0], @arguments[1]),
                                     :reorient => @arguments[2])
          else
            puts "Command: #{command} is not yet implemented, please try again."
            commands_reminder
          end
        end
      end

      return instruct
    end

    def extract_arguments(args, delimiter)
      return if args.nil?

      args = args.split(delimiter)

      direction = Common.cardinal_direction_to_deg(sanitise_input(args[2]))
      @arguments.push(args[0].to_i, args[1].to_i, Common.to_rad(direction))
    end

  end
end