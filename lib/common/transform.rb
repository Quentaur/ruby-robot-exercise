
module Common

  TWO_PI = 2 * Math::PI

  Vector2D = Struct.new(:x, :y)

  class Transform2D

    attr_accessor :position, :bearing

    def initialize(options={})
      @position = options[:position] || Vector2D.new(0,0)
	    @bearing = options[:bearing] || 0
    end

    def propagate(magnitude)
      next_x = @position.x + magnitude * Math.cos(bearing)
      next_y = @position.y + magnitude * Math.sin(bearing);
      return Vector2D.new(next_x, next_y)
    end
	
    def translate(magnitude)
      @position.x += magnitude * Math.cos(bearing);
      @position.y += magnitude * Math.sin(bearing);
    end

    def rotate(angle)
      turn = angle % TWO_PI
      @bearing = (bearing + turn + TWO_PI) % TWO_PI
    end

  end
  
end
