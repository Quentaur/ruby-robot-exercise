module Common

  EPSILON = 1e-6 #0.000001

  def self.float_compare (a, b, tolerance=EPSILON)
    (a-b).abs < tolerance
  end

  def self.to_rad(angle)
    (angle / 180.0.to_f) * Math::PI
  end

  def self.to_deg(angle)
    (angle * 180.0.to_f) / Math::PI
  end

  def self.rad_to_cardinal_direction (bearing)
    deg = to_deg(bearing)
    deg_to_cardinal_direction(deg).to_s.upcase
  end

  def self.deg_to_cardinal_direction(angle)
    case angle
    when 338..360 then :east
    when 0..22    then :east
    when 23..67   then :northeast
    when 68..112  then :north
    when 113..157 then :northwest
    when 158..202 then :west
    when 203..247 then :southwest
    when 248..292 then :south
    when 293..337 then :southeast

    else raise "ERROR: deg_to_cardinal_direction unhandled angle #{angle}"
    end
  end

  def self.cardinal_direction_to_deg(direction)
    case direction.to_sym
    when :east      then 0
    when :northeast then 45
    when :north     then 90
    when :northwest then 135
    when :west      then 180
    when :southwest then 225
    when :south     then 270
    when :southeast then 315

    else raise "Unknown direction #{direction}"
    end
  end

  def method_missing(unknown_input, *args, &block)
    raise "direction: #{unknown_input} is not yet implemented, define it in #{__FILE__}"
  end

end
