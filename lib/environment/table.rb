require_relative "../common/transform.rb"

module Environment
  class Table

    attr_reader :origin, :length, :width

    def initialize(options={})
      default_dimension = 5
      #the origin represents the minimum point on the table; where both x and y are at a minimum.
      corner = options[:origin] || Common::Vector2D.new(0,0)
      l = validate_dimension options[:length] || default_dimension
      w = validate_dimension options[:width]  || default_dimension

      @origin = corner
      @length = l
      @max_length = corner.y + l
      @width  = w
      @max_width = corner.x + w
    end

    def is_point_on_table(p)
      (p.x >= @origin.x && p.x <= @max_width) && (p.y >= @origin.y && p.y <= @max_length)
    end

    private

    def validate_dimension(length)
      return nil unless length 
      length.abs
    end

  end
end
